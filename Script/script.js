// ## Задание
// Реализовать функцию, которая будет добавлять список, введенный пользователем, на страницу.

let amountOfItems = Number(prompt('Enter a wishful amount of listitems', '2'));
let itemsList = new Array;
let ulList = document.createElement('ul');

for (let i = 0; i < amountOfItems; i++) {
    let itemText = prompt('Enter item text', '');
    itemsList.push(itemText);
    console.log(itemsList);
};

let values = itemsList.map((elem) => {
    return `<li>${elem}</li>`
});

let scriptTag = document.getElementsByTagName('script')[0];
document.body.insertBefore(ulList, scriptTag);

let ul = document.getElementsByTagName('ul')[0];
for (let index = 0; index < amountOfItems; index++) {
    ul.insertAdjacentHTML('beforeend', values[index]);
};

// Remove <ul/> in 10 seconds:
// setTimeout(() => {
//     document.body.removeChild(ulList);
//   }, 10000);

for (let id = 0; id < amountOfItems; id++) {
    var timerId = setTimeout(function tick() {
        let li = document.getElementsByTagName('li');
        ulList.removeChild(li[id]);
        timerId = setTimeout(tick, 2000);
    }, 2000);
};